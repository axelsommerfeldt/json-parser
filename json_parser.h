/**
 * @file json_parser.h
 * @brief Low-level JSON parser
 * @author Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @copyright 2023 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @version 2023-11-26
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#ifndef DULL_JSON_PARSER_H
#define DULL_JSON_PARSER_H

#include <stdexcept>
#include <cctype>

namespace dull
{

class json_input_stream1
{
	const char *data_;
	size_t length_;

public:
	json_input_stream1() noexcept : data_ { nullptr }, length_ { 0 } {}
	json_input_stream1( const char *data, size_t length ) noexcept : data_ { data }, length_ { length } {}

	void assign( const char *data, size_t length ) noexcept { data_ = data; length_ = length; }
	void clear() noexcept { data_ = nullptr; length_ = 0; }

	const char *data() const noexcept { return data_; }
	size_t length() const noexcept { return length_; }

	const char *tell() const noexcept { return data_; }
	bool eof() const noexcept { return length_ == 0; }

	char peek() const noexcept { return *data_; }
	char get() noexcept { length_--; return *data_++; }
	void unget() noexcept { --data_; ++length_; }

	void ignore() noexcept { data_++; length_--; }
};

class json_input_stream2
{
	const char *data_, *end_of_data_;

public:
	json_input_stream2() noexcept : data_ { nullptr }, end_of_data_ { nullptr } {}
	json_input_stream2( const char *data, size_t length ) noexcept : data_ { data }, end_of_data_ { data + length } {}

	void assign( const char *data, size_t length ) noexcept { data_ = data; end_of_data_ = data + length; }
	void clear() noexcept { data_ = end_of_data_ = nullptr; }

	const char *data() const noexcept { return data_; }
	size_t length() const noexcept { return end_of_data_ - data_; }

	const char *tell() const noexcept { return data_; }
	bool eof() const noexcept { return data_ == end_of_data_; }

	char peek() const noexcept { return *data_; }
	char get() noexcept { return *data_++; }
	void unget() noexcept { --data_; }

	void ignore() noexcept { data_++; }
};

class json_exception : public std::logic_error
{
public:
	explicit json_exception( const std::string &what_arg ) : std::logic_error( what_arg ) {}
	explicit json_exception( const char *what_arg ) : std::logic_error( what_arg ) {}
};

template< typename InputStream = json_input_stream2 >
class fast_json_parser_ : public InputStream
{
public:
	fast_json_parser_() noexcept {}
	fast_json_parser_( const char *data, size_t length ) noexcept : InputStream( data, length ) {}

	// Note: We don't support \" in member names
	void skip_name()
	{
		do
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [name]" );
		}
		while ( InputStream::get() != '"' );
	}

	void skip_value()
	{
		auto ch = InputStream::get();

		switch ( ch )
		{
		case '{':
			skip_object();
			break;
		case '[':
			skip_array();
			break;
		case '"':
			skip_string();
			break;
		default:
			skip_other( ch );
		}
	}

private:
	void skip_object()
	{
		auto ch = get_non_whitespace();
		if ( ch != '}' ) for (;;)
		{
			// Member name
			if ( ch != '"' )
				throw json_exception( "Malformed JSON: '\"' expected [object]" );
			skip_name();

			// Separator
			if ( get_non_whitespace() != ':' )
				throw json_exception( "Malformed JSON: ':' expected [object]" );

			// Member value
			skip_whitespace();
			skip_value();

			ch = get_non_whitespace();
			if ( ch == '}' ) return;
			if ( ch != ',' )
				throw json_exception( "Malformed JSON: ',' expected [object]" );

			ch = get_non_whitespace();
		}
	}

	void skip_array()
	{
		auto ch = get_non_whitespace();
		if ( ch != ']' ) for (;;)
		{
			InputStream::unget();

			// Element value
			skip_whitespace();
			skip_value();

			ch = get_non_whitespace();
			if ( ch == ']' ) return;
			if ( ch != ',' )
				throw json_exception( "Malformed JSON: ',' expected [array]" );

			get_non_whitespace();
		}
	}

	void skip_string()
	{
		for (;;)
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [string]" );

			auto ch = InputStream::get();
			if ( ch == '"' ) break;

			if ( ch == '\\' )
			{
				if ( InputStream::eof() )
					throw json_exception( "Malformed JSON: premature EOF [string]" );

				InputStream::ignore();
			}
		}
	}

	void skip_other( char ch )
	{
		for (;;)
		{
			if ( ch == ',' || ch == '}' || ch == ']' || is_whitespace( ch ) )
			{
				InputStream::unget();
				break;
			}

			if ( InputStream::eof() ) break;
			ch = InputStream::get();
		}
	}

public:
	char get_non_whitespace()
	{
		char ch;

		do
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [whitespace]" );

			ch = InputStream::get();
		}
		while ( is_whitespace( ch ) );

		return ch;
	}

	void skip_whitespace()
	{
		for (;;)
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [whitespace]" );

			if ( !is_whitespace( InputStream::peek() ) ) break;
			InputStream::ignore();
		}
	}

	static bool is_whitespace( char ch ) noexcept { return static_cast<unsigned char>( ch ) <= static_cast<unsigned char>( ' ' ); }
	static bool is_digit( char ch ) noexcept { return ch >= '0' && ch <= '9'; }
};

using fast_json_parser = fast_json_parser_<>;

template< typename InputStream = json_input_stream2 >
class strict_json_parser_ : public InputStream
{
public:
	strict_json_parser_() noexcept {}
	strict_json_parser_( const char *data, size_t length ) noexcept : InputStream( data, length ) {}

	// Note: We don't support \" in member names
	void skip_name()
	{
		for (;;)
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [name]" );

			auto ch = InputStream::get();
			if ( ch == '"' ) break;

			if ( !is_character( ch ) )
				throw json_exception( "Malformed JSON: invalid string character ASCII " + std::to_string( ch ) + "' [name]" );
		}
	}

	void skip_value()
	{
		auto ch = InputStream::get();

		switch ( ch )
		{
		case '{':
			skip_object();
			break;
		case '[':
			skip_array();
			break;
		case '"':
			skip_string();
			break;
		case 't':
			skip_true();
			break;
		case 'f':
			skip_false();
			break;
		case 'n':
			skip_null();
			break;
		default:
			skip_number( ch );
		}
	}

private:
	void skip_object()
	{
		auto ch = get_non_whitespace();
		if ( ch != '}' ) for (;;)
		{
			// Member name
			if ( ch != '"' )
				throw json_exception( "Malformed JSON: '\"' expected [object]" );
			skip_name();

			// Separator
			if ( get_non_whitespace() != ':' )
				throw json_exception( "Malformed JSON: ':' expected [object]" );

			// Member value
			skip_whitespace();
			skip_value();

			ch = get_non_whitespace();
			if ( ch == '}' ) return;
			if ( ch != ',' )
				throw json_exception( "Malformed JSON: ',' expected [object]" );

			ch = get_non_whitespace();
		}
	}

	void skip_array()
	{
		auto ch = get_non_whitespace();
		if ( ch != ']' ) for (;;)
		{
			InputStream::unget();

			// Element value
			skip_whitespace();
			skip_value();

			ch = get_non_whitespace();
			if ( ch == ']' ) return;
			if ( ch != ',' )
				throw json_exception( "Malformed JSON: ',' expected [array]" );

			get_non_whitespace();
		}
	}

	void skip_string()
	{
		for (;;)
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [string]" );

			auto ch = InputStream::get();
			if ( ch == '"' ) break;

			if ( ch == '\\' )
			{
				if ( InputStream::eof() )
					throw json_exception( "Malformed JSON: premature EOF [string]" );

				ch = InputStream::get();
				if ( ch != '\"' && ch != '\\' && ch != '/' && ch != 'b' && ch != 'f' && ch != 'n' && ch != 'r' && ch != 't' && ch != 'u' )
					throw json_exception( std::string( "Malformed JSON: invalid string escape '\\" ) + ch + "' [string]" );

				if ( ch == 'u' )
				{
					for ( int i = 4; --i >= 0; )
					{
						if ( InputStream::eof() )
							throw json_exception( "Malformed JSON: premature EOF [string]" );

						if ( !is_xdigit( ch = InputStream::get() ) )
							throw json_exception( std::string( "Malformed JSON: invalid hex digit '" ) + ch + "' [string]" );
					}

				}
			}
			else if ( !is_character( ch ) )
			{
				throw json_exception( "Malformed JSON: invalid string character ASCII " + std::to_string( ch ) + "' [string]" );
			}
		}
	}

	void skip_number( char ch )
	{
		// Sign
		if ( ch == '-' )
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [number]" );

			ch = InputStream::get();
		}

		// Digits
		if ( ch == '0' )
		{
			if ( InputStream::eof() ) return;
			ch = InputStream::peek();
		}
		else if ( is_digit( ch ) )
		{
			ch = skip_digits();
		}
		else
		{
			throw json_exception( std::string( "Malformed JSON: digit instead of '" ) + ch + "' expected [number]" );
		}

		// Fraction
		if ( ch == '.' )
		{
			InputStream::ignore();

			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [number]" );

			if ( !is_digit( (ch = InputStream::get()) ) )
				throw json_exception( std::string( "Malformed JSON: digit instead of '" ) + ch + "' expected [number]" );

			ch = skip_digits();
		}

		// Exponent
		if ( ch == 'E' || ch == 'e' )
		{
			InputStream::ignore();

			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [number]" );

			ch = InputStream::peek();
			if ( ch == '+' || ch == '-' )
			{
				InputStream::ignore();

				if ( InputStream::eof() )
					throw json_exception( "Malformed JSON: premature EOF [number]" );

				ch = InputStream::peek();
			}

			if ( !is_digit( ch ) )
				throw json_exception( std::string( "Malformed JSON: digit instead of '" ) + ch + "' expected [number]" );

			ch = skip_digits();
		}

		if ( ch != '\0' && ch != ',' && ch != '}' && ch != ']' && !is_whitespace( ch ) )
			throw json_exception( std::string( "Malformed JSON: delimiter instead of '" ) + ch + "' expected [number]" );
	}

	void skip_true()
	{
		for ( char ch : { 'r', 'u', 'e' } )
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [true]" );
			if ( InputStream::get() != ch )
				throw json_exception( "Malformed JSON: \"true\" expected [true]" );
		}

		if ( !InputStream::eof() )
		{
			auto ch = InputStream::peek();
			if ( ch != ',' && ch != '}' && ch != ']' && !is_whitespace( ch ) )
				throw json_exception( std::string( "Malformed JSON: delimiter instead of '" ) + ch + "' expected [true]" );
		}
	}

	void skip_false()
	{
		for ( char ch : { 'a', 'l', 's', 'e' } )
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [false]" );
			if ( InputStream::get() != ch )
				throw json_exception( "Malformed JSON: \"false\" expected [false]" );
		}

		if ( !InputStream::eof() )
		{
			auto ch = InputStream::peek();
			if ( ch != ',' && ch != '}' && ch != ']' && !is_whitespace( ch ) )
				throw json_exception( std::string( "Malformed JSON: delimiter instead of '" ) + ch + "' expected [false]" );
		}
	}

	void skip_null()
	{
		for ( char ch : { 'u', 'l', 'l' } )
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [null]" );
			if ( InputStream::get() != ch )
				throw json_exception( "Malformed JSON: \"null\" expected [null]" );
		}

		if ( !InputStream::eof() )
		{
			auto ch = InputStream::peek();
			if ( ch != ',' && ch != '}' && ch != ']' && !is_whitespace( ch ) )
				throw json_exception( std::string( "Malformed JSON: delimiter instead of '" ) + ch + "' expected [null]" );
		}
	}

private:
	char skip_digits()
	{
		char ch;

		for (;;)
		{
			if ( InputStream::eof() ) { ch = '\0'; break; }
			if ( !is_digit( ch = InputStream::peek() ) ) break;
			InputStream::ignore();
		}

		return ch;
	}

public:
	char get_non_whitespace()
	{
		char ch;

		do
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [whitespace]" );

			ch = InputStream::get();
		}
		while ( is_whitespace( ch ) );

		return ch;
	}

	void skip_whitespace()
	{
		for (;;)
		{
			if ( InputStream::eof() )
				throw json_exception( "Malformed JSON: premature EOF [whitespace]" );

			if ( !is_whitespace( InputStream::peek() ) ) break;
			InputStream::ignore();
		}
	}

	static bool is_whitespace( char ch ) noexcept { return ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t'; }
	static bool is_character( char ch ) noexcept { return static_cast<unsigned char>( ch ) >= static_cast<unsigned char>( ' ' ); }
	static bool is_digit( char ch ) noexcept { return std::isdigit( ch ); }
	static bool is_xdigit( char ch ) noexcept { return std::isxdigit( ch ); }
};

using strict_json_parser = strict_json_parser_<>;

}

#endif

