/**
 * @file json_object.h
 * @brief Parsing JSON objects
 * @author Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @copyright 2023 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @version 2023-11-26
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#ifndef DULL_JSON_OBJECT_H
#define DULL_JSON_OBJECT_H

#include "json_value.h"

namespace dull
{

class json_object
{
public:
	/* Constructor */

	json_object() noexcept : json_object( "{}", 2 ) {}
	json_object( const char *data, size_t length ) noexcept : storage_( data, length ), value_( storage_.c_str(), storage_.length() ) {}
	explicit json_object( const std::string &str ) noexcept : storage_( str.data(), str.length() ), value_( storage_.c_str(), storage_.length() ) {}
	explicit json_object( std::string &&str ) noexcept : storage_( std::move( str ) ), value_( storage_.c_str(), storage_.length() ) {}
	explicit json_object( const json_value &value ) { assign( value ); }

	json_object( const json_object &other ) noexcept : json_object( other.storage_ ) {}
	json_object( json_object &&other ) noexcept : json_object( std::move( other.storage_ ) ) { other.clear(); }

	/* Assignment */

	void assign( const char *data, size_t length ) noexcept { storage_.assign( data, length ); value_.assign( storage_.c_str(), storage_.length() ); }
	void assign( const std::string &str ) noexcept { storage_.assign( str.data(), str.length() ); value_.assign( storage_.c_str(), storage_.length() ); }
	void assign( std::string &&str ) noexcept { storage_ = std::move( str ); value_.assign( storage_.c_str(), storage_.length() ); }
	void assign( const json_value &value ) { auto str = value.value(); assign( str.data(), str.length() ); }

	json_object &operator=( const json_object &other ) noexcept { assign( other.storage_ ); return *this; }
	json_object &operator=( json_object &&other ) noexcept { assign( std::move( other.storage_ ) ); other.clear(); return *this; }

	/* Element access */

	const std::string &value() const
	{
		return storage_;
	}

	json_value operator[]( const dull::string_view &name ) const
	{
		return value_[name];
	}

	/* Iterators */

	using iterator = typename json_value::iterator;
	using const_iterator = typename json_value::const_iterator;

	iterator begin() const { return value_.begin(); }
	iterator end() const { return value_.end(); }

	/* Capacity */

	bool empty() const
	{
		return begin() == end();
	}

	size_t size() const
	{
		size_t n = 0;
		for ( auto it = begin(); it != end(); ++it ) n++;
		return n;
	}

	/* Modifiers */

	void clear() noexcept { assign( "{}", 2 ); }

	/* JSON validation */

	template< typename P = strict_json_parser >
	bool valid() const noexcept
	{
		return value_.is_object() && value_.template valid<P>();
	}

private:
	std::string storage_;
	json_value value_;
};

}

#endif

