/**
 * @file from_chars.h
 * @brief Integer and floating point parser using default ("C") locale
 * @author Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @copyright 2023 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @version 2023-10-29
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#ifndef DULL_FROM_CHARS_H
#define DULL_FROM_CHARS_H

// Since C++17 std::from_chars() is available, making this solution obsolete
#if __cplusplus >= 201703L
#pragma message "std::from_chars() should be used instead"
#endif

#include <atomic>
#include <mutex>
#include <locale>
#include <ios>

namespace dull
{

template< typename InputIt >
inline std::locale from_chars_locale()
{
	static volatile std::atomic<bool> x;
	static std::mutex m;
	static std::locale l;

	if ( !x.load() )
	{
		std::lock_guard<std::mutex> guard( m );
		if ( !x.load() )
		{
			// Construct a copy of std::locale::classic() except for the facet of type std::num_get< char, string_view::iterator >
			l = std::locale( std::locale::classic(), new std::num_get< char, InputIt > );
			x.store( true );
		}
	}

	return l;
}

template< typename InputIt, typename T >
inline bool from_chars( InputIt first, InputIt last, T &value )
{
	// This code was taken from https://en.cppreference.com/w/cpp/locale/num_get
	// but we use string_view (or std::string) instead of std::istringstream since it's about 3 times faster.

	struct ios_base : public std::ios_base {} s3;
	s3.imbue( from_chars_locale<InputIt>() );

	std::ios::iostate err = std::ios_base::goodbit;
	std::use_facet< std::num_get< char, InputIt > >( s3.getloc() ).get( first, last, s3, err, value );
	return err == std::ios_base::eofbit;  // true if no failure occurred and all characters have been used, false otherwise
}

// Since std::stoX() is about 4 times faster than std::num_get::get() we add template specializations for all available std::stoX() where X = fixed point integer
// (We have to do so for "int" anyway since std::num_get::get() does not support "int". As sufficient justice, there is no std::stoui() available :-( )

template< typename InputIt >
inline bool from_chars( InputIt first, InputIt last, int &value )
{
	std::string str( first, last );
	size_t pos {};

	value = std::stoi( str, &pos );
	return !str.empty() && pos == str.length();
}

template< typename InputIt >
inline bool from_chars( InputIt first, InputIt last, long &value )
{
	std::string str( first, last );
	size_t pos {};

	value = std::stol( str, &pos );
	return !str.empty() && pos == str.length();
}

template< typename InputIt >
inline bool from_chars( InputIt first, InputIt last, long long &value )
{
	std::string str( first, last );
	size_t pos {};

	value = std::stoll( str, &pos );
	return !str.empty() && pos == str.length();
}

template< typename InputIt >
inline bool from_chars( InputIt first, InputIt last, unsigned long &value )
{
	std::string str( first, last );
	size_t pos {};

	value = std::stoul( str, &pos );
	return !str.empty() && pos == str.length();
}

template< typename InputIt >
inline bool from_chars( InputIt first, InputIt last, unsigned long long &value )
{
	std::string str( first, last );
	size_t pos {};

	value = std::stoull( str, &pos );
	return !str.empty() && pos == str.length();
}

}

#endif

