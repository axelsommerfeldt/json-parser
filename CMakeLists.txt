cmake_minimum_required( VERSION 3.7...3.27 )

project( json-parser
	VERSION 0.10.0
#	DESCRIPTION "Simple JSON parser"
#	HOMEPAGE_URL "https://gitlab.com/axelsommerfeldt/json-parser"
	LANGUAGES CXX
	)

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.9" )
	set( CMAKE_PROJECT_DESCRIPTION "Simple JSON parser" )
	set( PROJECT_DESCRIPTION "${CMAKE_PROJECT_DESCRIPTION}" )
endif()

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.12" )
	set( CMAKE_PROJECT_HOMEPAGE_URL "https://gitlab.com/axelsommerfeldt/json-parser" )
	set( PROJECT_HOMEPAGE_URL "${CMAKE_PROJECT_HOMEPAGE_URL}" )
endif()

enable_testing()

add_library( json-parser INTERFACE )
set_target_properties( json-parser PROPERTIES CXX_STANDARD 11 CXX_EXTENSIONS OFF CXX_STANDARD_REQUIRED ON )
target_sources( json-parser INTERFACE json_object.h json_value.h json_parser.h string_view.h from_chars.h )
target_include_directories( json-parser INTERFACE . )

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.8" )
	target_compile_features( json-parser INTERFACE cxx_std_11 )
endif()

message( STATUS "Fetching googletest from github" )

include( FetchContent )
FetchContent_Declare( googletest
	GIT_REPOSITORY https://github.com/google/googletest.git
	GIT_TAG v1.14.0
)
FetchContent_MakeAvailable( googletest )

message( STATUS "Fetching googletest from github - done" )

include( GoogleTest )
add_executable( json-parser-gtest test.cpp )
target_compile_options( json-parser-gtest PRIVATE -Wall -Wextra -Wconversion -pedantic )
target_link_libraries( json-parser-gtest PRIVATE json-parser gtest_main )
#gtest_add_tests( TARGET json-parser-gtest )
gtest_discover_tests( json-parser-gtest )

# Source code documentation

find_package( Doxygen )
if( Doxygen_FOUNDx )
	set( DOXYGEN_INPUT_DIR ${PROJECT_SOURCE_DIR} )
	set( DOXYGEN_SOURCE_FILES json_object.h json_value.h json_parser.h )
	set( DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR} )
	set( DOXYGEN_INDEX_FILE ${DOXYGEN_OUTPUT_DIR}/html/index.html )

	set( DOXYFILE_IN  ${CMAKE_CURRENT_LIST_DIR}/Doxyfile.in )
	set( DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile )
	configure_file( ${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY )

	add_custom_command(
		OUTPUT ${DOXYGEN_INDEX_FILE}
		COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
		MAIN_DEPENDENCY ${DOXYFILE_OUT} ${DOXYFILE_IN}
		DEPENDS README.md ${DOXYGEN_SOURCE_FILES}
		WORKING_DIRECTORY ${DOXYGEN_INPUT_DIR}
		COMMENT "Generating source code documentation"
	#	VERBATIM
		)

	add_custom_target( doxygen DEPENDS ${DOXYGEN_INDEX_FILE} )
endif()

