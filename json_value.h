/**
 * @file json_value.h
 * @brief Parsing JSON values
 * @author Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @copyright 2023 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @version 2023-12-28
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#ifndef DULL_JSON_VALUE_H
#define DULL_JSON_VALUE_H

#include "json_parser.h"

#include <string>
#include <utility>
#include <cstdlib>

#if __cplusplus >= 201703L
#include <string_view>
#include <charconv>
namespace dull { using string_view = std::string_view; }
#else
#include "string_view.h"
#include "from_chars.h"
#endif

namespace dull
{

enum class json_type { Undefined = 0, Object, Array, String, Number, True, False, Null, EndOfObject, EndOfArray };

inline std::string to_string( json_type type )
{
	switch ( type )
	{
	case json_type::Undefined:
		return "undefined";
	case json_type::Object:
		return "object";
	case json_type::Array:
		return "array";
	case json_type::String:
		return "string";
	case json_type::Number:
		return "number";
	case json_type::True:
	case json_type::False:
		return "boolean";
	case json_type::Null:
		return "null";
	case json_type::EndOfObject:
		return "end_of_object";
	case json_type::EndOfArray:
		return "end_of_array";
	default:
		return "unknown";
	}
}

class json_iterator;

class json_value
{
	using Parser = fast_json_parser;

public:
	/* Constructor */

	json_value() noexcept {}
	json_value( const char *data, size_t length ) noexcept : parser_( data, length ) {}
	explicit json_value( const dull::string_view &str ) noexcept : parser_( str.data(), str.length() ) {}

	/* Assignment */

	void assign( const char *data, size_t length ) noexcept { parser_.assign( data, length ); }
	void assign( const dull::string_view &str ) noexcept { parser_.assign( str.data(), str.length() ); }

	/* JSON type */

	json_type type() const noexcept
	{
		return parser_.eof() ? json_type::Undefined : type( parser_.peek() );
	}

	bool is_undefined() const noexcept { return parser_.eof(); }

	bool is_object() const noexcept { return !parser_.eof() && parser_.peek() == '{'; }
	bool is_array() const noexcept { return !parser_.eof() && parser_.peek() == '['; }
	bool is_string() const noexcept { return !parser_.eof() && parser_.peek() == '"'; }
	bool is_number() const noexcept { auto ch = parser_.eof() ? '\0' : parser_.peek(); return ch == '-' || parser_.is_digit( ch ); }
	bool is_boolean() const noexcept { return !parser_.eof() && (parser_.peek() == 't' || parser_.peek() == 'f'); }
	bool is_true() const noexcept { return !parser_.eof() && parser_.peek() == 't'; }
	bool is_false() const noexcept { return !parser_.eof() && parser_.peek() == 'f'; }
	bool is_null() const noexcept { return !parser_.eof() && parser_.peek() == 'n'; }
	bool end_of_object() const noexcept { return !parser_.eof() && parser_.peek() == '}'; }
	bool end_of_array() const noexcept { return !parser_.eof() && parser_.peek() == ']'; }

	bool is_structure() const noexcept { return !parser_.eof() && (parser_.peek() == '{' || parser_.peek() == '['); }
	bool end_of_structure() const noexcept { return !parser_.eof() && (parser_.peek() == '}' || parser_.peek() == ']'); }
	bool is_primitive() const noexcept { auto ch = parser_.eof() ? '\0' : parser_.peek(); return dull::string_view( "\"-0123456789tfn", 15 ).find( ch ) != dull::string_view::npos; }

	/* Element access */

	dull::string_view value() const
	{
		if ( parser_.eof() ) return {};

		return raw_value();
	}

	dull::string_view contents() const
	{
		if ( parser_.eof() ) return {};

		switch ( parser_.peek() )
		{
		case '{':
		case '[':
			return trim( raw_value_without_punctuation_marks() );
		case '"':
			return raw_value_without_punctuation_marks();
		case 'n':
			return {};
		default:
			return raw_value();
		}
	}

	json_value operator[]( const dull::string_view &name ) const;
	json_value operator[]( size_t index ) const;

	std::string string() const
	{
		if ( !is_string() )
			throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [string]" );

		return string_value( raw_value_without_punctuation_marks() );
	}

	std::string string( const dull::string_view &default_value ) const
	{
		return is_undefined() ? std::string( default_value.data(), default_value.length() ) : string();
	}

	dull::string_view string_view() const
	{
		if ( !is_string() )
			throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [string]" );

		return raw_value_without_punctuation_marks();
	}

	dull::string_view string_view( const dull::string_view &default_value ) const
	{
		return is_undefined() ? default_value : string_view();
	}

	template< typename T = int > T number() const
	{
		if ( !is_number() )
			throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [number]" );

		return number_value<T>( raw_value() );
	}

	template< typename T = int > T number( T default_value ) const
	{
		return is_undefined() ? default_value : number<T>();
	}

	bool boolean() const
	{
		if ( is_true() )
			return true;
		if ( is_false() )
			return false;

		throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [boolean]" );
	}

	bool boolean( bool default_value ) const
	{
		return is_undefined() ? default_value : boolean();
	}

	template< typename T >
	T as() const
	{
		T result;

		switch ( type() )
		{
		case json_type::Number:
			result = number_value<T>( raw_value() );
			break;
		case json_type::String:
			result = number_value<T>( raw_value_without_punctuation_marks() );
			break;
		case json_type::True:
			result = 1;
			break;
		default:
			result = 0;
		}

		return result;
	}

	template< typename T, typename DefaultValue = T >
	T as( DefaultValue default_value ) const
	{
		return is_undefined() ? default_value : as<T>();
	}

	/* Iterators */

	using iterator = json_iterator;
	using const_iterator = json_iterator;

	iterator begin() const;
	iterator end() const;

	iterator first() const;

	/* Capacity */

	bool empty() const
	{
		switch ( type() )
		{
		case json_type::Undefined:
		case json_type::Null:
			return true;
		case json_type::Object:
		case json_type::Array:
			return object_or_array_empty();
		case json_type::String:
			return string_empty();
		default:
			return false;
		}
	}

	size_t size() const
	{
		switch ( type() )
		{
		case json_type::Object:
		case json_type::Array:
			return object_or_array_size();
		case json_type::String:
			return string_size();
		case json_type::Number:
		case json_type::True:
		case json_type::False:
			return 1;
		default:
			return 0;
		}
	}

	/* Modifiers */

	void clear() noexcept { parser_.clear(); }

	/* JSON validation */

	template< typename P = strict_json_parser >
	void validate() const
	{
		if ( parser_.eof() )
			throw json_exception( "Malformed JSON: Value expected [validate]" );

		P parser( parser_.data(), parser_.length() );
		parser.skip_value();
	}

	template< typename P = strict_json_parser >
	bool valid() const noexcept
	{
		try
		{
			validate<P>();
			return true;
		}
		catch ( const json_exception & )
		{
			return false;
		}
	}

protected:
	bool object_or_array_empty() const;
	size_t object_or_array_size() const;

	bool string_empty() const
	{
		auto parser = parser_;
		parser.ignore();  // skip '"'
		if ( parser.eof() )
			throw json_exception( "Malformed JSON: premature EOF [string_empty]" );
		return parser.peek() == '"';
	}

	size_t string_size() const
	{
		auto parser = parser_;
		parser.ignore();  // skip '"'

		for ( size_t n = 0;; )
		{
			if ( parser.eof() )
				throw json_exception( "Malformed JSON: premature EOF [string_size]" );

			auto ch = parser.get();
			if ( ch == '"' ) return n;

			if ( ch == '\\' )
			{
				if ( parser.eof() )
					throw json_exception( "Malformed JSON: premature EOF [string_size]" );

				ch = parser.get();
				if ( ch == 'u' )
				{
					auto pos = parser.tell();

					for ( int i = 4; --i >= 0; )
					{
						if ( parser.eof() )
							throw json_exception( "Malformed JSON: premature EOF [string_size]" );
						parser.ignore();
					}

					n += unicode_to_mbs( { pos, 4 }, nullptr, 0 );
					continue;
				}
			}

			n++;
		}
	}

	static std::string string_value( dull::string_view str )
	{
		auto pos = str.find( '\\' );
		std::string result;

		if ( pos == dull::string_view::npos )
		{
			result.assign( str.data(), str.length() );
		}
		else
		{
			// Since we know that we will replace at least one "\x"
			// the resulting string will have a length <= str.length() - 1.
			result.reserve( str.length() - 1 );

			do
			{
				result.append( str.data(), pos++ );
				auto ch = str[pos++];
				str.remove_prefix( pos );

				switch ( ch )
				{
				case 'b':
					ch = '\b';
					break;
				case 'f':
					ch = '\f';
					break;
				case 'n':
					ch = '\n';
					break;
				case 'r':
					ch = '\r';
					break;
				case 't':
					ch = '\t';
					break;
				case 'u':
				{
					char buf[8];
					unicode_to_mbs( str, buf, sizeof buf );
					str.remove_prefix( 4 );
					result.append( buf );
					continue;
				}
				default:
					;
				}

				result += ch;
			} while ( (pos = str.find( '\\' )) != dull::string_view::npos );

			result.append( str.data(), str.length() );
		}

		return result;
	}

	template< typename T >
	static T number_value( dull::string_view str )
	{
		T result;

	#if __cplusplus >= 201703L
		auto ec = std::from_chars( str.data(), str.data() + str.length(), result ).ec;
		if ( ec != std::errc() )
	#else
		if ( !dull::from_chars( str.begin(), str.end(), result ) )
	#endif
			throw std::range_error( "JSON: \"" + std::string( str.data(), str.length() ) + "\" is not a valid number [number]" );

		return result;
	}

private:
	static json_type type( char ch )
	{
		switch ( ch )
		{
		case '{':
			return json_type::Object;
		case '[':
			return json_type::Array;
		case '"':
			return json_type::String;
		case 't':
			return json_type::True;
		case 'f':
			return json_type::False;
		case 'n':
			return json_type::Null;
		case '}':
			return json_type::EndOfObject;
		case ']':
			return json_type::EndOfArray;
		default:
			return json_type::Number;
		}
	}

	dull::string_view raw_value() const
	{
		auto parser = parser_;
		parser.skip_value();
		return { parser_.tell(), static_cast<size_t>( parser.tell() - parser_.tell() ) };
	}

	dull::string_view raw_value_without_punctuation_marks() const
	{
		auto parser = parser_;
		parser.skip_value();
		return { parser_.tell() + 1, static_cast<size_t>( (parser.tell() - 1) - (parser_.tell() + 1) ) };
	}

	static dull::string_view trim( dull::string_view str )
	{
		while ( !str.empty() && Parser::is_whitespace( str[0] ) ) str.remove_prefix( 1 );
		while ( !str.empty() && Parser::is_whitespace( str[str.length()-1] ) ) str.remove_suffix( 1 );
		return str;
	}

	static size_t unicode_to_mbs( const dull::string_view &str, char *dst, size_t len )
	{
		if ( str.length() < 4 )
			throw json_exception( "Malformed JSON: premature EOF [unicode_to_mbs]" );

		char u[5] = { str[0], str[1], str[2], str[3], '\0' };

		char *pos;
		wchar_t w[2] = { static_cast<wchar_t>( std::strtoul( u, &pos, 16 ) ), L'\0' };
		if ( pos != u + 4 )
			throw json_exception( std::string( "Malformed JSON: invalid hex digits \"" ) + u + "\" [unicode_to_mbs]" );

		const wchar_t *src = w;
		std::mbstate_t ps = {};
		auto n = std::wcsrtombs( dst, &src, len, &ps );
		if ( n == static_cast<std::size_t>(-1) )
		{
			// std::cerr << "WARNING: json_value: unable to convert \\u" << u << "\n";
			if ( dst != nullptr ) { dst[0] = '?'; dst[1] = '\0'; }
			n = 1;
		}

		return n;
	}

private:
	Parser parser_;

	friend class json_iterator;
};

template <>
inline std::string json_value::as<std::string>() const
{
	std::string result;

	if ( is_string() )
	{
		result = string_value( raw_value_without_punctuation_marks() );
	}
	else if ( !is_undefined() )
	{
		auto v = raw_value();
		result.assign( v.data(), v.length() );
	}

	return result;
}

template <>
inline std::string json_value::as<std::string, const dull::string_view& >( const dull::string_view &default_value ) const
{
	return is_undefined() ? std::string( default_value.data(), default_value.length() ) : as<std::string>();
}

template <>
inline dull::string_view json_value::as<dull::string_view>() const
{
	dull::string_view result;

	if ( is_string() )
		result = raw_value_without_punctuation_marks();
	else if ( !is_undefined() )
		result = raw_value();

	return result;
}

template <>
inline bool json_value::as<bool>() const
{
	bool result;

	switch ( type() )
	{
	case json_type::String:
		result = !string_empty();
		break;
	case json_type::Number: {
		auto v = raw_value();
		result = v != "0" && v != "-0"; }
		break;
	case json_type::True:
		result = true;
		break;
	default:
		result = false;
	}

	return result;
}

class json_iterator
{
	using Parser = fast_json_parser;

public:
	using iterator_category = std::forward_iterator_tag;
	using value_type = std::pair< dull::string_view, json_value >;
	using pointer = const value_type *;
	using reference = const value_type &;
	using difference_type = std::ptrdiff_t;

	json_iterator() noexcept {}

	explicit json_iterator( Parser parser, bool clear_iterator_at_end = true )
	{
		first( parser, clear_iterator_at_end );
	}

	json_iterator &operator++()
	{
		next( true );
		return *this;
	}

	json_iterator operator++( int )
	{
		auto retval = *this;
		next( true );
		return retval;
	}

	json_iterator &next()
	{
		next( false );
		return *this;
	}

	json_iterator &next( const json_iterator &end_of_structure )
	{
		value_.second = end_of_structure.value_.second;

		auto ch = value_.second.parser_.get();
		if ( ch != '}' && ch != ']' )
			throw json_exception( "Unexpected JSON type '" + to_string( json_value::type( ch ) ) + "' [next]" );

		next_value( false );
		return *this;
	}

	bool operator==( const json_iterator &other ) const { return value_.second.parser_.tell() == other.value_.second.parser_.tell(); }
	bool operator!=( const json_iterator &other ) const { return value_.second.parser_.tell() != other.value_.second.parser_.tell(); }

	reference operator*() { return value_; }
	pointer operator->() { return &value_; }

private:
	void parse_member_name( Parser &parser )
	{
		auto pos = parser.tell();
		parser.skip_name();
		value_.first = { pos, static_cast<size_t>( (parser.tell() - 1) - pos ) };

		if ( parser.get_non_whitespace() != ':' ) throw json_exception( "Malformed JSON: ':' expected" );

		parser.skip_whitespace();
	}

	void first( Parser &parser, bool clear_iterator_at_end )
	{
		if ( parser.eof() ) return;

		auto ch = parser.get();
		if ( ch == '{' )
		{
			// JSON object

			// Test for empty object
			if ( (ch = parser.get_non_whitespace()) == '}' )
			{
				if ( clear_iterator_at_end ) return;
				parser.unget();
			}
			else
			{
				// Parse (and remember) member name, and skip delimiter between member name and value
				if ( ch != '"' ) throw json_exception( "Malformed JSON: '\"' expected [first]" );
				parse_member_name( parser );
			}
		}
		else if ( ch == '[' )
		{
			// JSON array

			// Test for empty array
			if ( parser.get_non_whitespace() == ']' && clear_iterator_at_end ) return;

			// Set parser to 1st value or ']'
			parser.unget();
		}
		else
		{
			throw json_exception( "Unexpected JSON type '" + to_string( json_value::type( ch ) ) + "' [first]" );
		}

		value_.second.parser_ = parser;
	}

	void next( bool clear_iterator_at_end )
	{
		value_.second.parser_.skip_value();
		next_value( clear_iterator_at_end );
	}

	void next_value( bool clear_iterator_at_end )
	{
		auto &parser = value_.second.parser_;

		auto ch = parser.get_non_whitespace();
		if ( ch == ',' )
		{
			if ( value_.first.data() )
			{
				// JSON object
				ch = parser.get_non_whitespace();
				if ( ch != '"' ) throw json_exception( "Malformed JSON: '\"' expected [next]" );
				parse_member_name( parser );
			}
			else
			{
				// JSON array
				parser.skip_whitespace();
			}
		}
		else if ( ch == '}' || ch == ']' )
		{
			if ( clear_iterator_at_end ) parser.clear();
			else { value_.first = {}; parser.unget(); }
		}
		else
		{
			throw json_exception( value_.first.data()
					? "Malformed JSON: ',' or '}' expected [next]"
					: "Malformed JSON: ',' or ']' expected [next]" );
		}
	}

private:
	value_type value_;
};

inline json_value::iterator json_value::begin() const { return iterator( parser_ ); }
inline json_value::iterator json_value::end() const { return iterator(); }

inline json_value::iterator json_value::first() const { return iterator( parser_, false ); }

inline bool json_value::object_or_array_empty() const { return begin() == end(); }
inline size_t json_value::object_or_array_size() const { size_t n = 0; for ( auto it = begin(); it != end(); ++it ) n++; return n; }

inline json_value json_value::operator[]( const dull::string_view &name ) const
{
	if ( !is_undefined() )
	{
		if ( !is_object() )
			throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [member]" );

		// cppcheck-suppress useStlAlgorithm
		for ( const auto &member : *this ) if ( member.first == name ) return member.second;
	}

	return {};
}

inline json_value json_value::operator[]( size_t index ) const
{
	if ( !is_undefined() )
	{
		if ( !is_array() )
			throw json_exception( "Unexpected JSON type '" + to_string( type() ) + "' [element]" );

		auto i = index;
		for ( const auto &element : *this ) if ( i-- == 0 ) return element.second;

		throw std::out_of_range( "JSON array index " + std::to_string( index ) + " out of range [element]" );
	}

	return {};
}

}

#endif

