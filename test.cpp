#include "json_object.h"
#include <gtest/gtest.h>

//#define JSON_INPUT_STREAM_SPEED_TEST        // If defined a pseudo speed test will be done
#define LOCALE ""                             // Locale needed to convert "\u00b0" into "\xC2\xB0" ("" = user's preferred locale)
#define U00B0_CONVERTED_TO_LOCALE "\xC2\xB0"

using namespace dull;

const char strings[] = R"({"test1":"\b\f\n\r\t\u00b0\\\""})";
const char numbers[] = R"({"test1":-1234,"test2":-1.234,"test3":-1.23e10,"test4":"1234"})";

/* Examples taken from https://json.org/example.html */

const char example1[] = R"({
    "glossary": {
        "title": "example glossary",
        "GlossDiv": {
            "title": "S",
            "GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
                    "SortAs": "SGML",
                    "GlossTerm": "Standard Generalized Markup Language",
                    "Acronym": "SGML",
                    "Abbrev": "ISO 8879:1986",
                    "GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
                        "GlossSeeAlso": ["GML", "XML"]
                    },
                    "GlossSee": "markup"
                }
            }
        }
    }
})";

const char example2[] = R"#({"menu": {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}})#";

const char example3[] = R"({"widget": {
    "debug": "on",
    "window": {
        "title": "Sample Konfabulator Widget",
        "name": "main_window",
        "width": 500,
        "height": 500
    },
    "image": { 
        "src": "Images/Sun.png",
        "name": "sun1",
        "hOffset": 250,
        "vOffset": 250,
        "alignment": "center"
    },
    "text": {
        "data": "Click Here",
        "size": 36,
        "style": "bold",
        "name": "text1",
        "hOffset": 250,
        "vOffset": 100,
        "alignment": "center",
        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
    }
}})";

const char example4[] = R"({"web-app": {
  "servlet": [   
    {
      "servlet-name": "cofaxCDS",
      "servlet-class": "org.cofax.cds.CDSServlet",
      "init-param": {
        "configGlossary:installationAt": "Philadelphia, PA",
        "configGlossary:adminEmail": "ksm@pobox.com",
        "configGlossary:poweredBy": "Cofax",
        "configGlossary:poweredByIcon": "/images/cofax.gif",
        "configGlossary:staticPath": "/content/static",
        "templateProcessorClass": "org.cofax.WysiwygTemplate",
        "templateLoaderClass": "org.cofax.FilesTemplateLoader",
        "templatePath": "templates",
        "templateOverridePath": "",
        "defaultListTemplate": "listTemplate.htm",
        "defaultFileTemplate": "articleTemplate.htm",
        "useJSP": false,
        "jspListTemplate": "listTemplate.jsp",
        "jspFileTemplate": "articleTemplate.jsp",
        "cachePackageTagsTrack": 200,
        "cachePackageTagsStore": 200,
        "cachePackageTagsRefresh": 60,
        "cacheTemplatesTrack": 100,
        "cacheTemplatesStore": 50,
        "cacheTemplatesRefresh": 15,
        "cachePagesTrack": 200,
        "cachePagesStore": 100,
        "cachePagesRefresh": 10,
        "cachePagesDirtyRead": 10,
        "searchEngineListTemplate": "forSearchEnginesList.htm",
        "searchEngineFileTemplate": "forSearchEngines.htm",
        "searchEngineRobotsDb": "WEB-INF/robots.db",
        "useDataStore": true,
        "dataStoreClass": "org.cofax.SqlDataStore",
        "redirectionClass": "org.cofax.SqlRedirection",
        "dataStoreName": "cofax",
        "dataStoreDriver": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
        "dataStoreUrl": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon",
        "dataStoreUser": "sa",
        "dataStorePassword": "dataStoreTestQuery",
        "dataStoreTestQuery": "SET NOCOUNT ON;select test='test';",
        "dataStoreLogFile": "/usr/local/tomcat/logs/datastore.log",
        "dataStoreInitConns": 10,
        "dataStoreMaxConns": 100,
        "dataStoreConnUsageLimit": 100,
        "dataStoreLogLevel": "debug",
        "maxUrlLength": 500}},
    {
      "servlet-name": "cofaxEmail",
      "servlet-class": "org.cofax.cds.EmailServlet",
      "init-param": {
      "mailHost": "mail1",
      "mailHostOverride": "mail2"}},
    {
      "servlet-name": "cofaxAdmin",
      "servlet-class": "org.cofax.cds.AdminServlet"},
 
    {
      "servlet-name": "fileServlet",
      "servlet-class": "org.cofax.cds.FileServlet"},
    {
      "servlet-name": "cofaxTools",
      "servlet-class": "org.cofax.cms.CofaxToolsServlet",
      "init-param": {
        "templatePath": "toolstemplates/",
        "log": 1,
        "logLocation": "/usr/local/tomcat/logs/CofaxTools.log",
        "logMaxSize": "",
        "dataLog": 1,
        "dataLogLocation": "/usr/local/tomcat/logs/dataLog.log",
        "dataLogMaxSize": "",
        "removePageCache": "/content/admin/remove?cache=pages&id=",
        "removeTemplateCache": "/content/admin/remove?cache=templates&id=",
        "fileTransferFolder": "/usr/local/tomcat/webapps/content/fileTransferFolder",
        "lookInContext": 1,
        "adminGroupID": 4,
        "betaServer": true}}],
  "servlet-mapping": {
    "cofaxCDS": "/",
    "cofaxEmail": "/cofaxutil/aemail/*",
    "cofaxAdmin": "/admin/*",
    "fileServlet": "/static/*",
    "cofaxTools": "/tools/*"},
 
  "taglib": {
    "taglib-uri": "cofax.tld",
    "taglib-location": "/WEB-INF/tlds/cofax.tld"}}})";

const char example5[] = R"({"menu": {
    "header": "SVG Viewer",
    "items": [
        {"id": "Open"},
        {"id": "OpenNew", "label": "Open New"},
        null,
        {"id": "ZoomIn", "label": "Zoom In"},
        {"id": "ZoomOut", "label": "Zoom Out"},
        {"id": "OriginalView", "label": "Original View"},
        null,
        {"id": "Quality"},
        {"id": "Pause"},
        {"id": "Mute"},
        null,
        {"id": "Find", "label": "Find..."},
        {"id": "FindAgain", "label": "Find Again"},
        {"id": "Copy"},
        {"id": "CopyAgain", "label": "Copy Again"},
        {"id": "CopySVG", "label": "Copy SVG"},
        {"id": "ViewSVG", "label": "View SVG"},
        {"id": "ViewSource", "label": "View Source"},
        {"id": "SaveAs", "label": "Save As"},
        null,
        {"id": "Help"},
        {"id": "About", "label": "About Adobe CVG Viewer..."}
    ]
}})";

static bool CI()
{
	auto ci = getenv( "CI" );
	return ci != nullptr && strcmp( ci, "true" ) == 0;
}

TEST( json_value, value )
{
	json_value json;
	EXPECT_EQ( json.value(), "" );

	json.assign( strings );
	EXPECT_EQ( json["test1"].value(), "\"\\b\\f\\n\\r\\t\\u00b0\\\\\\\"\"" );

	json.assign( numbers );
	EXPECT_EQ( json["test1"].value(), "-1234" );
	EXPECT_EQ( json["test2"].value(), "-1.234" );
	EXPECT_EQ( json["test3"].value(), "-1.23e10" );
	EXPECT_EQ( json["test4"].value(), "\"1234\"" );

	json.assign( example1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].value(), "\"SGML\"" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"].value(), "[\"GML\", \"XML\"]" );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].value(), "false" );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].value(), "true" );
	EXPECT_EQ( servlet[4]["init-param"]["log"].value(), "1" );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items[1]["label"].value(), "\"Open New\"" );
	EXPECT_EQ( menu_items[2].value(), "null" );
}

TEST( json_value, contents )
{
	json_value json;
	EXPECT_EQ( json.contents(), "" );

	json.assign( R"({  "name": "value"    })" );
	EXPECT_EQ( json.contents(), R"("name": "value")" );

	json.assign( strings );
	EXPECT_EQ( json["test1"].contents(), "\\b\\f\\n\\r\\t\\u00b0\\\\\\\"" );

	json.assign( numbers );
	EXPECT_EQ( json["test1"].contents(), "-1234" );
	EXPECT_EQ( json["test2"].contents(), "-1.234" );
	EXPECT_EQ( json["test3"].contents(), "-1.23e10" );
	EXPECT_EQ( json["test4"].contents(), "1234" );

	json.assign( example1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].contents(), "SGML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"].contents(), "\"GML\", \"XML\"" );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].contents(), "false" );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].contents(), "true" );
	EXPECT_EQ( servlet[4]["init-param"]["log"].contents(), "1" );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items[1]["label"].contents(), "Open New" );
	EXPECT_EQ( menu_items[2].contents(), "" );
}

TEST( json_value, type )
{
	json_value json;
	EXPECT_EQ( json.type(), json_type::Undefined );

	json.assign( strings );
	EXPECT_EQ( json.type(), json_type::Object );
	EXPECT_EQ( json["test1"].type(), json_type::String );

	json.assign( numbers );
	EXPECT_EQ( json.type(), json_type::Object );
	EXPECT_EQ( json["test1"].type(), json_type::Number );
	EXPECT_EQ( json["test2"].type(), json_type::Number );
	EXPECT_EQ( json["test3"].type(), json_type::Number );
	EXPECT_EQ( json["test4"].type(), json_type::String );

	json.assign( example1 );
	EXPECT_EQ( json.type(), json_type::Object );
	EXPECT_EQ( json["glossary"].type(), json_type::Object );
	EXPECT_EQ( json["glossary"]["GlossDiv"].type(), json_type::Object );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"].type(), json_type::Object );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"].type(), json_type::Object );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].type(), json_type::String );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"].type(), json_type::Array );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].type(), json_type::String );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].type(), json_type::String );
	EXPECT_EQ( json["test"].type(), json_type::Undefined );

	json.assign( example2 );
	EXPECT_EQ( json.type(), json_type::Object );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].type(), json_type::String );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].type(), json_type::String );

	json.assign( example3 );
	EXPECT_EQ( json.type(), json_type::Object );
	EXPECT_EQ( json["widget"]["window"]["height"].type(), json_type::Number );
	EXPECT_EQ( json["widget"]["text"]["onMouseUp"].type(), json_type::String );

	json.assign( example4 );
	EXPECT_EQ( json.type(), json_type::Object );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet.type(), json_type::Array );
	EXPECT_EQ( servlet[0]["init-param"]["templateOverridePath"].type(), json_type::String );
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].type(), json_type::False );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].type(), json_type::True );
	EXPECT_EQ( servlet[4]["init-param"]["log"].type(), json_type::Number );

	json.assign( example5 );
	EXPECT_EQ( json.type(), json_type::Object );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items.type(), json_type::Array );
	EXPECT_EQ( menu_items[1].type(), json_type::Object );
	EXPECT_EQ( menu_items[1]["label"].type(), json_type::String );
	EXPECT_EQ( menu_items[2].type(), json_type::Null );
}

TEST( json_value, string )
{
	json_value json;

	json.assign( strings );
	if ( CI() )
	{
		EXPECT_EQ( json["test1"].string(), "\b\f\n\r\t?\\\"" );
	}
	else
	{
		setlocale( LC_ALL, LOCALE );
		EXPECT_EQ( json["test1"].string(), "\b\f\n\r\t" U00B0_CONVERTED_TO_LOCALE "\\\"" );
	}

	json.assign( numbers );
	EXPECT_EQ( json["test4"].string(), "1234" );

	json.assign( example1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].string(), "SGML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].string(), "GML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].string(), "XML" );

	json.assign( example2 );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].string(), "Open" );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].string(), "CloseDoc()" );

	json.assign( example3 );
	EXPECT_EQ( json["widget"]["text"]["onMouseUp"].string(), "sun1.opacity = (sun1.opacity / 100) * 90;" );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["templateOverridePath"].string(), "" );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items[1]["label"].string(), "Open New" );
	EXPECT_EQ( menu_items[1]["X"].string( "undefined" ), "undefined" );
}

TEST( json_value, string_view )
{
	json_value json;

	json.assign( strings );
	EXPECT_EQ( json["test1"].string_view(), R"(\b\f\n\r\t\u00b0\\\")" );

	json.assign( numbers );
	EXPECT_EQ( json["test4"].string_view(), "1234" );

	json.assign( example1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].string_view(), "SGML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].string_view(), "GML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].string_view(), "XML" );

	json.assign( example2 );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].string_view(), "Open" );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].string_view(), "CloseDoc()" );

	json.assign( example3 );
	EXPECT_EQ( json["widget"]["text"]["onMouseUp"].string_view(), "sun1.opacity = (sun1.opacity / 100) * 90;" );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["templateOverridePath"].string_view(), "" );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items[1]["label"].string_view(), "Open New" );
	EXPECT_EQ( menu_items[1]["X"].string_view( "undefined" ), "undefined" );
}

TEST( json_value, number )
{
	json_value json;

	json.assign( numbers );
	EXPECT_EQ( json["test1"].number(), -1234 );
	EXPECT_EQ( json["test2"].number<float>(), -1.234f );
	EXPECT_EQ( json["test2"].number<double>(), -1.234 );
	EXPECT_EQ( json["test3"].number<long double>(), -1.23e10 );

	bool is_number = true;
	try { json["test4"].number(); }
	catch ( ... ) { is_number = false; }
	EXPECT_FALSE( is_number );

	json.assign( example3 );
	EXPECT_EQ( json["widget"]["window"]["height"].number<unsigned>(), 500 );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[4]["init-param"]["log"].number(), 1 );
	EXPECT_EQ( servlet[4]["init-param"]["X"].number( 10 ), 10 );
}

TEST( json_value, boolean )
{
	json_value json;

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].boolean(), false );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].boolean(), true );
	EXPECT_EQ( servlet[0]["init-param"]["X"].boolean( true ), true );
}

TEST( json_value, as )
{
	json_value json;
	EXPECT_EQ( json.as<std::string>(), "" );
	EXPECT_EQ( json.as<std::string>( "undefined" ), "undefined" );
	EXPECT_EQ( json.as<string_view>(), "" );
	EXPECT_EQ( json.as<string_view>( "undefined" ), "undefined" );

	json.assign( numbers );
	EXPECT_EQ( json["test1"].as<std::string>(), "-1234" );
	EXPECT_EQ( json["test1"].as<string_view>(), "-1234" );
	EXPECT_EQ( json["test1"].as<int>(), -1234 );
	EXPECT_EQ( json["test2"].as<std::string>(), "-1.234" );
	EXPECT_EQ( json["test2"].as<string_view>(), "-1.234" );
	EXPECT_EQ( json["test2"].as<float>(), -1.234f );
	EXPECT_EQ( json["test2"].as<double>(), -1.234 );
	EXPECT_EQ( json["test3"].as<std::string>(), "-1.23e10" );
	EXPECT_EQ( json["test3"].as<string_view>(), "-1.23e10" );
	EXPECT_EQ( json["test3"].as<long double>(), -1.23e10 );
	EXPECT_EQ( json["test4"].as<std::string>(), "1234" );
	EXPECT_EQ( json["test4"].as<string_view>(), "1234" );
	EXPECT_EQ( json["test4"].as<unsigned>(), 1234 );

	json.assign( example1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].as<std::string>(), "SGML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].as<std::string>(), "GML" );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].as<std::string>(), "XML" );

	json.assign( example2 );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].as<std::string>(), "Open" );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].as<std::string>(), "CloseDoc()" );

	json.assign( example3 );
	EXPECT_EQ( json["widget"]["window"]["height"].as<std::string>(), "500" );
	EXPECT_EQ( json["widget"]["window"]["height"].as<size_t>(), 500 );
	EXPECT_EQ( json["widget"]["text"]["onMouseUp"].as<std::string>(), "sun1.opacity = (sun1.opacity / 100) * 90;" );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet[0]["init-param"]["templateOverridePath"].as<std::string>(), "" );
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].as<std::string>(), "false" );
	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].as<bool>(), false );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].as<std::string>(), "true" );
	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].as<bool>(), true );
	EXPECT_EQ( servlet[4]["init-param"]["log"].as<std::string>(), "1" );
	EXPECT_EQ( servlet[4]["init-param"]["log"].as<int>(), 1 );
	EXPECT_EQ( servlet[4]["init-param"]["log"].as<bool>(), true );
	EXPECT_EQ( servlet[0]["init-param"]["log"].as<bool>(), false );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items[1]["label"].as<std::string>(), "Open New" );

	EXPECT_EQ( menu_items[1]["X"].as<std::string>( "undefined" ), "undefined" );
	EXPECT_EQ( servlet[4]["init-param"]["X"].as<int>( 10 ), 10 );
	EXPECT_EQ( servlet[0]["init-param"]["X"].as<bool>( true ), true );
}

TEST( json_value, iterator_1 )
{
	json_value json;
	EXPECT_EQ( json.begin(), json.end() );

	json.assign( "{}" );
	EXPECT_EQ( json.begin(), json.end() );

	json.assign( "{ }" );
	EXPECT_EQ( json.begin(), json.end() );

	json.assign( strings );
	auto it = json.begin();
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( json.begin()->first, "test1" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"\\b\\f\\n\\r\\t\\u00b0\\\\\\\"\"" );
	++it;
	EXPECT_EQ( it, json.end() );

	json.assign( numbers );
	it = json.begin();
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test1" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1234" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test2" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1.234" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test3" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1.23e10" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test4" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"1234\"" );
	++it;
	EXPECT_EQ( it, json.end() );

	json.assign( example1 );
	it = json.begin();
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "glossary" );
	ASSERT_TRUE( it->second.is_object() );
	++it;
	EXPECT_EQ( it, json.end() );

	json.assign( example1 );
	auto it1 = json.begin()->second.begin();
	ASSERT_NE( it1, json.end() );
	EXPECT_EQ( it1->first, "title" );
	EXPECT_TRUE( it1->second.is_string() );
	EXPECT_EQ( it1->second.value(), "\"example glossary\"" );
	++it1;
	ASSERT_NE( it1, json.end() );
	EXPECT_EQ( it1->first, "GlossDiv" );
	ASSERT_TRUE( it1->second.is_object() );
	auto it2 = it1->second.begin();
	ASSERT_NE( it2, json.end() );
	EXPECT_EQ( it2->first, "title" );
	EXPECT_TRUE( it2->second.is_string() );
	EXPECT_EQ( it2->second.value(), "\"S\"" );
	++it2;
	ASSERT_NE( it2, json.end() );
	EXPECT_EQ( it2->first, "GlossList" );
	ASSERT_TRUE( it2->second.is_object() );
	auto it3 = it2->second.begin();
	ASSERT_NE( it3, json.end() );
	EXPECT_EQ( it3->first, "GlossEntry" );
	ASSERT_TRUE( it3->second.is_object() );
	auto it4 = it3->second.begin();
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "ID" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"SGML\"" );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "SortAs" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"SGML\"" );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "GlossTerm" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"Standard Generalized Markup Language\"" );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "Acronym" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"SGML\"" );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "Abbrev" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"ISO 8879:1986\"" );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "GlossDef" );
	ASSERT_TRUE( it4->second.is_object() );
	auto it5 = it4->second.begin();
	ASSERT_NE( it5, json.end() );
	EXPECT_EQ( it5->first, "para" );
	EXPECT_TRUE( it5->second.is_string() );
	EXPECT_EQ( it5->second.value(), "\"A meta-markup language, used to create markup languages such as DocBook.\"" );
	++it5;
	ASSERT_NE( it5, json.end() );
	EXPECT_EQ( it5->first, "GlossSeeAlso" );
	ASSERT_TRUE( it5->second.is_array() );
	EXPECT_EQ( it5->second.value(), "[\"GML\", \"XML\"]" );
	auto it6 = it5->second.begin();
	ASSERT_NE( it6, json.end() );
	EXPECT_TRUE( it6->first.empty() );  // array members don't have a name
	EXPECT_TRUE( it6->second.is_string() );
	EXPECT_EQ( it6->second.value(), "\"GML\"" );
	++it6;
	ASSERT_NE( it6, json.end() );
	EXPECT_TRUE( it6->first.empty() );  // array members don't have a name
	EXPECT_TRUE( it6->second.is_string() );
	EXPECT_EQ( it6->second.value(), "\"XML\"" );
	++it6;
	EXPECT_EQ( it6, json.end() );
	++it5;
	EXPECT_EQ( it5, json.end() );
	++it4;
	ASSERT_NE( it4, json.end() );
	EXPECT_EQ( it4->first, "GlossSee" );
	EXPECT_TRUE( it4->second.is_string() );
	EXPECT_EQ( it4->second.value(), "\"markup\"" );
	++it4;
	EXPECT_EQ( it4, json.end() );
	++it3;
	EXPECT_EQ( it3, json.end() );
	++it2;
	EXPECT_EQ( it2, json.end() );
	++it1;
	EXPECT_EQ( it1, json.end() );

	json.assign( example2 );
	it = json.begin();
	EXPECT_NE( it, json.end() );
	EXPECT_EQ( it->first, "menu" );
	EXPECT_TRUE( it->second.is_object() );
	++it;
	EXPECT_EQ( it, json.end() );

	json.assign( example3 );
	it = json.begin();
	EXPECT_NE( it, json.end() );
	EXPECT_EQ( it->first, "widget" );
	EXPECT_TRUE( it->second.is_object() );
	++it;
	EXPECT_EQ( it, json.end() );

	json.assign( example4 );
	it = json.begin();
	EXPECT_NE( it, json.end() );
	EXPECT_EQ( it->first, "web-app" );
	ASSERT_TRUE( it->second.is_object() );
	++it;
	EXPECT_EQ( it, json.end() );
	it = json.begin()->second.begin();
	EXPECT_EQ( it->first, "servlet" );
	ASSERT_TRUE( it->second.is_array() );
	it = it->second.begin();
	EXPECT_TRUE( it->first.empty() );  // array members don't have a name
	ASSERT_TRUE( it->second.is_object() );
	it = it->second.begin();
	EXPECT_EQ( it->first, "servlet-name" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"cofaxCDS\"" );
	++it;
	EXPECT_EQ( it->first, "servlet-class" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"org.cofax.cds.CDSServlet\"" );
	++it;
	EXPECT_EQ( it->first, "init-param" );
	ASSERT_TRUE( it->second.is_object() );
	it = it->second.begin();
	EXPECT_EQ( it->first, "configGlossary:installationAt" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"Philadelphia, PA\"" );
	++it;
	EXPECT_EQ( it->first, "configGlossary:adminEmail" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"ksm@pobox.com\"" );
	++it;
	EXPECT_EQ( it->first, "configGlossary:poweredBy" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"Cofax\"" );
	++it;
	EXPECT_EQ( it->first, "configGlossary:poweredByIcon" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"/images/cofax.gif\"" );
	++it;
	EXPECT_EQ( it->first, "configGlossary:staticPath" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"/content/static\"" );
	++it;
	EXPECT_EQ( it->first, "templateProcessorClass" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"org.cofax.WysiwygTemplate\"" );
	++it;
	EXPECT_EQ( it->first, "templateLoaderClass" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"org.cofax.FilesTemplateLoader\"" );
	++it;
	EXPECT_EQ( it->first, "templatePath" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"templates\"" );
	++it;
	EXPECT_EQ( it->first, "templateOverridePath" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"\"" );
	++it;
	EXPECT_EQ( it->first, "defaultListTemplate" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"listTemplate.htm\"" );
	++it;
	EXPECT_EQ( it->first, "defaultFileTemplate" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"articleTemplate.htm\"" );
	++it;
	EXPECT_EQ( it->first, "useJSP" );
	EXPECT_TRUE( it->second.is_boolean() );
	EXPECT_EQ( it->second.value(), "false" );

	json.assign( example5 );
	it = json.begin();
	EXPECT_NE( it, json.end() );
	EXPECT_EQ( it->first, "menu" );
	++it;
	EXPECT_EQ( it, json.end() );
	it = json.begin();
	ASSERT_TRUE( it->second.is_object() );
	it = it->second.begin();
	EXPECT_EQ( it->first, "header" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"SVG Viewer\"" );
	++it;
	EXPECT_EQ( it->first, "items" );
	ASSERT_TRUE( it->second.is_array() );
	it = it->second.begin();
	EXPECT_TRUE( it->first.empty() );  // array members don't have a name
	EXPECT_TRUE( it->second.is_object() );
	EXPECT_EQ( it->second.value(), "{\"id\": \"Open\"}" );
	++it;
	EXPECT_TRUE( it->first.empty() );  // array members don't have a name
	EXPECT_TRUE( it->second.is_object() );
	EXPECT_EQ( it->second.value(), "{\"id\": \"OpenNew\", \"label\": \"Open New\"}" );
	++it;
	EXPECT_TRUE( it->first.empty() );  // array members don't have a name
	EXPECT_TRUE( it->second.is_null() );
	EXPECT_EQ( it->second.value(), "null" );
}

TEST( json_value, first_next_1 )
{
	json_value json( "{ }" );
	EXPECT_TRUE( json.is_object() );
	EXPECT_TRUE( json.empty() );
	EXPECT_EQ( json.size(), 0 );
	auto it = json.first();
	EXPECT_EQ( it->first, "" );
	EXPECT_TRUE( it->second.end_of_object() );

	json.assign( example1 );
	auto glossary = json["glossary"];
	ASSERT_TRUE( glossary.is_object() );
	EXPECT_EQ( glossary.size(), 2 );

	it = glossary.first();
	EXPECT_EQ( it->first, "title" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.string(), "example glossary" );
	it = it.next();
	EXPECT_EQ( it->first, "GlossDiv" );
	EXPECT_TRUE( it->second.is_object() );
	it = it.next();
	EXPECT_EQ( it->first, "" );
	EXPECT_TRUE( it->second.end_of_object() );

	auto GlossSeeAlso = glossary["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"];
	ASSERT_TRUE( GlossSeeAlso.is_array() );
	EXPECT_EQ( GlossSeeAlso.size(), 2 );

	it = GlossSeeAlso.first();
	EXPECT_TRUE( it->first.empty() );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.string(), "GML" );
	it = it.next();
	EXPECT_TRUE( it->first.empty() );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.string(), "XML" );
	it = it.next();
	EXPECT_TRUE( it->first.empty() );
	EXPECT_TRUE( it->second.end_of_array() );
}

struct parse_result
{
	parse_result() { clear(); }
	void clear() { depth = max_depth = 0; number_of_objects = number_of_arrays = number_of_strings = number_of_numbers = number_of_booleans = number_of_nulls = 0; }

	int depth, max_depth;
	int number_of_objects, number_of_arrays, number_of_strings, number_of_numbers, number_of_booleans, number_of_nulls;
};

void parse1( const json_value &parent, parse_result &result )
{
	if ( result.max_depth < ++result.depth ) result.max_depth = result.depth;

	for ( auto it = parent.begin(); it != parent.end(); ++it )
	{
		switch ( it->second.type() )
		{
		case json_type::Object:
			result.number_of_objects++;
			parse1( it->second, result );
			break;
		case json_type::Array:
			result.number_of_arrays++;
			parse1( it->second, result );
			break;
		case json_type::String:
			result.number_of_strings++;
			break;
		case json_type::Number:
			result.number_of_numbers++;
			break;
		case json_type::True:
		case json_type::False:
			result.number_of_booleans++;
			break;
		case json_type::Null:
			result.number_of_nulls++;
			break;
		default:
			EXPECT_TRUE( false ) << "Invalid JSON type " << static_cast<int>(it->second.type()) << "\n";
		}
	}

	result.depth--;
}

json_iterator parse2( const json_value &parent, parse_result &result )
{
	if ( result.max_depth < ++result.depth ) result.max_depth = result.depth;

	json_iterator it = parent.first();
	while ( !it->second.end_of_structure() )
	{
		switch ( it->second.type() )
		{
		case json_type::Object:
			result.number_of_objects++;
			it.next( parse2( it->second, result ) );
			break;
		case json_type::Array:
			result.number_of_arrays++;
			it.next( parse2( it->second, result ) );
			break;
		case json_type::String:
			result.number_of_strings++;
			it.next();
			break;
		case json_type::Number:
			result.number_of_numbers++;
			it.next();
			break;
		case json_type::True:
		case json_type::False:
			result.number_of_booleans++;
			it.next();
			break;
		case json_type::Null:
			result.number_of_nulls++;
			it.next();
			break;
		default:
			EXPECT_TRUE( false ) << "Invalid JSON type " << static_cast<int>(it->second.type()) << "\n";
			return it;
		}
	}

	result.depth--;
	return it;
}

TEST( json_value, iterator_2 )
{
	parse_result r;

	parse1( json_value( example1 ), r );
	EXPECT_EQ( r.max_depth, 7 );
	EXPECT_EQ( r.number_of_objects, 5 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 11 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse1( json_value( example2 ), r );
	EXPECT_EQ( r.max_depth, 5 );
	EXPECT_EQ( r.number_of_objects, 5 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 8 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse1( json_value( example3 ), r );
	EXPECT_EQ( r.max_depth, 3 );
	EXPECT_EQ( r.number_of_objects, 4 );
	EXPECT_EQ( r.number_of_arrays, 0 );
	EXPECT_EQ( r.number_of_strings, 11 );
	EXPECT_EQ( r.number_of_numbers, 7 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse1( json_value( example4 ), r );
	EXPECT_EQ( r.max_depth, 5 );
	EXPECT_EQ( r.number_of_objects, 11 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 53 );
	EXPECT_EQ( r.number_of_numbers, 18 );
	EXPECT_EQ( r.number_of_booleans, 3 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse1( json_value( example5 ), r );
	EXPECT_EQ( r.max_depth, 4 );
	EXPECT_EQ( r.number_of_objects, 19 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 31 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 4 );
}

TEST( json_value, first_next_2 )
{
	parse_result r;

	parse2( json_value( example1 ), r );
	EXPECT_EQ( r.max_depth, 7 );
	EXPECT_EQ( r.number_of_objects, 5 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 11 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse2( json_value( example2 ), r );
	EXPECT_EQ( r.max_depth, 5 );
	EXPECT_EQ( r.number_of_objects, 5 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 8 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse2( json_value( example3 ), r );
	EXPECT_EQ( r.max_depth, 3 );
	EXPECT_EQ( r.number_of_objects, 4 );
	EXPECT_EQ( r.number_of_arrays, 0 );
	EXPECT_EQ( r.number_of_strings, 11 );
	EXPECT_EQ( r.number_of_numbers, 7 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse2( json_value( example4 ), r );
	EXPECT_EQ( r.max_depth, 5 );
	EXPECT_EQ( r.number_of_objects, 11 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 53 );
	EXPECT_EQ( r.number_of_numbers, 18 );
	EXPECT_EQ( r.number_of_booleans, 3 );
	EXPECT_EQ( r.number_of_nulls, 0 );

	r.clear();
	parse2( json_value( example5 ), r );
	EXPECT_EQ( r.max_depth, 4 );
	EXPECT_EQ( r.number_of_objects, 19 );
	EXPECT_EQ( r.number_of_arrays, 1 );
	EXPECT_EQ( r.number_of_strings, 31 );
	EXPECT_EQ( r.number_of_numbers, 0 );
	EXPECT_EQ( r.number_of_booleans, 0 );
	EXPECT_EQ( r.number_of_nulls, 4 );
}

TEST( json_value, empty )
{
	json_value json;
	EXPECT_TRUE( json.empty() );

	json.assign( example1 );
	EXPECT_FALSE( json.empty() );
	EXPECT_FALSE( json["glossary"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].empty() );
	EXPECT_FALSE( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].empty() );
	EXPECT_TRUE( json["test"].empty() );

	json.assign( example2 );
	EXPECT_FALSE( json.empty() );
	EXPECT_FALSE( json["menu"]["popup"]["menuitem"][1]["value"].empty() );
	EXPECT_FALSE( json["menu"]["popup"]["menuitem"][2]["onclick"].empty() );
	EXPECT_TRUE( json["test"].empty() );

	json.assign( example3 );
	EXPECT_FALSE( json.empty() );
	EXPECT_FALSE( json["widget"]["window"]["height"].empty() );
	EXPECT_FALSE( json["widget"]["text"]["onMouseUp"].empty() );
	EXPECT_TRUE( json["test"].empty() );

	json.assign( example4 );
	EXPECT_FALSE( json.empty() );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_FALSE( servlet.empty() );
	EXPECT_TRUE( servlet[0]["init-param"]["templateOverridePath"].empty() );
	EXPECT_FALSE( servlet[0]["init-param"]["useJSP"].empty() );
	EXPECT_FALSE( servlet[0]["init-param"]["useDataStore"].empty() );
	EXPECT_FALSE( servlet[4]["init-param"]["log"].empty() );

	json.assign( example5 );
	EXPECT_FALSE( json.empty() );
	auto menu_items = json["menu"]["items"];
	EXPECT_FALSE( menu_items.empty() );
	EXPECT_FALSE( menu_items[1].empty() );
	EXPECT_FALSE( menu_items[1]["label"].empty() );
	EXPECT_TRUE( menu_items[2].empty() );
}

TEST( json_value, size )
{
	json_value json;
	EXPECT_EQ( json.size(), 0 );

	json.assign( strings );

	if ( CI() )
	{
		EXPECT_EQ( json["test1"].size(), 8 );
	}
	else
	{
		setlocale( LC_ALL, LOCALE );
		EXPECT_EQ( json["test1"].size(), 9 );
	}

	json.assign( example1 );
	EXPECT_EQ( json.size(), 1 );
	EXPECT_EQ( json["glossary"].size(), 2 );
	EXPECT_EQ( json["glossary"]["GlossDiv"].size(), 2 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"].size(), 1 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"].size(), 7 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["ID"].size(), 4 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"].size(), 2 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][0].size(), 3 );
	EXPECT_EQ( json["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"][1].size(), 3 );
	EXPECT_EQ( json["test"].size(), 0 );

	json.assign( example2 );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].size(), 4 );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].size(), 10 );

	json.assign( example3 );
//	EXPECT_EQ( json["widget"]["window"]["height"].size(), 1 );
	EXPECT_EQ( json["widget"]["text"]["onMouseUp"].size(), 41 );

	json.assign( example4 );
	auto servlet = json["web-app"]["servlet"];
	EXPECT_EQ( servlet.size(), 5 );
	EXPECT_EQ( servlet[0]["init-param"]["templateOverridePath"].size(), 0 );
//	EXPECT_EQ( servlet[0]["init-param"]["useJSP"].size(), 1 );
//	EXPECT_EQ( servlet[0]["init-param"]["useDataStore"].size(), 1 );
//	EXPECT_EQ( servlet[4]["init-param"]["log"].size(), 1 );

	json.assign( example5 );
	auto menu_items = json["menu"]["items"];
	EXPECT_EQ( menu_items.size(), 22 );
	EXPECT_EQ( menu_items[1].size(), 2 );
	EXPECT_EQ( menu_items[1]["label"].size(), 8 );
	EXPECT_EQ( menu_items[2].size(), 0 );
}

TEST( json_value, valid )
{
	EXPECT_FALSE( json_value().valid() );

	EXPECT_TRUE( json_value( strings ).valid() );
	EXPECT_TRUE( json_value( numbers ).valid() );

	EXPECT_TRUE( json_value( example1 ).valid() );
	EXPECT_TRUE( json_value( example2 ).valid() );
	EXPECT_TRUE( json_value( example3 ).valid() );
	EXPECT_TRUE( json_value( example4 ).valid() );
	EXPECT_TRUE( json_value( example5 ).valid() );

	static const char bad_example1[] = R"({"test":0)";
	EXPECT_FALSE( json_value( bad_example1 ).valid() );

	static const char bad_example2[] = R"({"test":})";
	EXPECT_FALSE( json_value( bad_example2 ).valid() );

	static const char bad_example3[] = R"({"test":+1234})";
	EXPECT_FALSE( json_value( bad_example3 ).valid() );

	static const char bad_example4[] = R"({"test":01})";
	EXPECT_FALSE( json_value( bad_example4 ).valid() );

	static const char bad_example5[] = R"({"test":1.})";
	EXPECT_FALSE( json_value( bad_example5 ).valid() );

	static const char bad_example6[] = R"({"test":"\a"})";
	EXPECT_FALSE( json_value( bad_example6 ).valid() );

	static const char bad_example7[] = R"({"test":0}x)";
	EXPECT_TRUE( json_value( bad_example7 ).valid() );  // this gets undetected by strict_json_parser (garbage after end-of-JSON)

	EXPECT_TRUE( json_value( "{}" ).valid() );
	EXPECT_FALSE( json_value( "{" ).valid() );
	EXPECT_TRUE( json_value( "[]" ).valid() );
	EXPECT_FALSE( json_value( "[" ).valid() );
	EXPECT_TRUE( json_value( "\"\"" ).valid() );
	EXPECT_FALSE( json_value( "\"" ).valid() );
	EXPECT_FALSE( json_value( "-" ).valid() );
	EXPECT_TRUE( json_value( "0" ).valid() );
	EXPECT_FALSE( json_value( "1a" ).valid() );
	EXPECT_FALSE( json_value( "1." ).valid() );
	EXPECT_TRUE( json_value( "1.23" ).valid() );
	EXPECT_FALSE( json_value( "-1.23e" ).valid() );
	EXPECT_FALSE( json_value( "-1.23e+" ).valid() );
	EXPECT_FALSE( json_value( "-1.23e-" ).valid() );
	EXPECT_TRUE( json_value( "-1.23e+10" ).valid() );
	EXPECT_TRUE( json_value( "-1.23e-10" ).valid() );
	EXPECT_FALSE( json_value( "+1.23e-10" ).valid() );
	EXPECT_FALSE( json_value( "t" ).valid() );
	EXPECT_TRUE( json_value( "true" ).valid() );
	EXPECT_FALSE( json_value( "true1" ).valid() );
	EXPECT_FALSE( json_value( "f" ).valid() );
	EXPECT_TRUE( json_value( "false" ).valid() );
	EXPECT_FALSE( json_value( "false2" ).valid() );
	EXPECT_FALSE( json_value( "n" ).valid() );
	EXPECT_TRUE( json_value( "null" ).valid() );
	EXPECT_FALSE( json_value( "null3" ).valid() );
}

#ifdef JSON_INPUT_STREAM_SPEED_TEST

static constexpr size_t number_of_iterations = 100000;

template< typename Iterator >
void iterate_recursive( Iterator it )
{
	for ( ; !it->second.is_undefined(); ++it )
	{
		if ( it->second.is_object() || it->second.is_array() ) iterate_recursive( it->second.begin() );
	}
}

TEST( json_iterator, json_input_stream1 )
{
	for ( size_t i = 0; i < number_of_iterations; i++ )
	{
		fast_json_parser<json_input_stream1> parser( example1, sizeof( example1 ) );
		iterate_recursive( json_iterator_<fast_json_parser<json_input_stream1>>( parser ) );
	}
}

TEST( json_iterator, json_input_stream2 )
{
	for ( size_t i = 0; i < number_of_iterations; i++ )
	{
		fast_json_parser<json_input_stream2> parser( example1, sizeof( example1 ) );
		iterate_recursive( json_iterator_<fast_json_parser<json_input_stream2>>( parser ) );
	}
}

TEST( json_iterator, test_input_stream )
{
	class test_input_stream
	{
		dull::string_view str;

	public:
		test_input_stream() noexcept {}
		test_input_stream( const char *data, size_t length ) noexcept : str( data, length ) {}

		void clear() noexcept { str = { nullptr, 0 }; }

		const char *tell() const noexcept { return str.data(); }
		bool eof() const noexcept { return str.empty(); }

		char peek() const noexcept { return *str.data(); }
		char get() noexcept { char retval = *str.data(); str.remove_prefix( 1 ); return retval; }
		void unget() noexcept { str = { str.data() - 1, str.length() + 1 }; }

		void ignore() noexcept { str.remove_prefix( 1 ); }
	};

	for ( size_t i = 0; i < number_of_iterations; i++ )
	{
		fast_json_parser<test_input_stream> parser( example1, sizeof( example1 ) );
		iterate_recursive( json_iterator_<fast_json_parser<test_input_stream>>( parser ) );
	}
}

TEST( json_iterator, strict_json_input_stream1 )
{
	for ( size_t i = 0; i < number_of_iterations; i++ )
	{
		strict_json_parser<json_input_stream1> parser( example1, sizeof( example1 ) );
		iterate_recursive( json_iterator_<strict_json_parser<json_input_stream1>>( parser ) );
	}
}

TEST( json_iterator, strict_json_input_stream2 )
{
	for ( size_t i = 0; i < number_of_iterations; i++ )
	{
		strict_json_parser<json_input_stream2> parser( example1, sizeof( example1 ) );
		iterate_recursive( json_iterator_<strict_json_parser<json_input_stream2>>( parser ) );
	}
}

#endif

TEST( json_object, constructor )
{
	json_object json1;
	EXPECT_TRUE( json1.empty() );
	EXPECT_EQ( json1.size(), 0 );
	EXPECT_TRUE( json1.valid() );

	json_object json2( numbers, sizeof numbers - 1 );
	EXPECT_FALSE( json2.empty() );
	EXPECT_EQ( json2.size(), 4 );
	EXPECT_TRUE( json2.valid() );

	json_object json3( numbers );
	EXPECT_FALSE( json3.empty() );
	EXPECT_EQ( json3.size(), 4 );
	EXPECT_TRUE( json3.valid() );

	std::string str = numbers;
	json_object json4( str );
	EXPECT_FALSE( str.empty() );
	EXPECT_FALSE( json4.empty() );
	EXPECT_EQ( json4.size(), 4 );
	EXPECT_TRUE( json4.valid() );

	json_object json5( std::move( str ) );
	EXPECT_TRUE( str.empty() );
	EXPECT_FALSE( json5.empty() );
	EXPECT_EQ( json5.size(), 4 );
	EXPECT_TRUE( json5.valid() );

	json_value json6_value( example1 );
	json_object json6( json6_value["glossary"] );
	EXPECT_FALSE( json6.empty() );
	EXPECT_EQ( json6.size(), 2 );
	EXPECT_TRUE( json6.valid() );

	json_object json8( numbers );
	json_object json9( std::move( json8 ) );
	EXPECT_EQ( json8.value(), "{}" );
	EXPECT_TRUE( json8.empty() );
	EXPECT_EQ( json8.size(), 0 );
	EXPECT_TRUE( json8.valid() );
	EXPECT_NE( json9.value(), "{}" );
	EXPECT_FALSE( json9.empty() );
	EXPECT_EQ( json9.size(), 4 );
	EXPECT_TRUE( json9.valid() );
}

TEST( json_object, assignment )
{
	json_object json;

	json.assign( numbers, sizeof numbers - 1 );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 4 );
	EXPECT_TRUE( json.valid() );

	json.assign( numbers );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 4 );
	EXPECT_TRUE( json.valid() );

	std::string str = numbers;
	json.assign( str );
	EXPECT_FALSE( str.empty() );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 4 );
	EXPECT_TRUE( json.valid() );

	json.assign( std::move( str ) );
	EXPECT_TRUE( str.empty() );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 4 );
	EXPECT_TRUE( json.valid() );

	json.assign( json_value( example1 )["glossary"] );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 2 );
	EXPECT_TRUE( json.valid() );

	json_object json2;
	json = json2;
	EXPECT_TRUE( json.empty() );
	EXPECT_EQ( json.size(), 0 );
	EXPECT_TRUE( json.valid() );

	json_object json4( numbers );
	json = std::move( json4 );
	EXPECT_EQ( json4.value(), "{}" );
	EXPECT_TRUE( json4.empty() );
	EXPECT_EQ( json4.size(), 0 );
	EXPECT_TRUE( json4.valid() );
	EXPECT_NE( json.value(), "{}" );
	EXPECT_FALSE( json.empty() );
	EXPECT_EQ( json.size(), 4 );
	EXPECT_TRUE( json.valid() );
}

TEST( json_object, members )
{
	json_object json( example2 );

	EXPECT_EQ( json["menu"]["popup"]["menuitem"][1]["value"].string(), "Open" );
	EXPECT_EQ( json["menu"]["popup"]["menuitem"][2]["onclick"].string(), "CloseDoc()" );
}

TEST( json_object, iterator )
{
	json_object json( numbers );

	auto it = json.begin();
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test1" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1234" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test2" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1.234" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test3" );
	EXPECT_TRUE( it->second.is_number() );
	EXPECT_EQ( it->second.value(), "-1.23e10" );
	++it;
	ASSERT_NE( it, json.end() );
	EXPECT_EQ( it->first, "test4" );
	EXPECT_TRUE( it->second.is_string() );
	EXPECT_EQ( it->second.value(), "\"1234\"" );
	++it;
	EXPECT_EQ( it, json.end() );
}

TEST( json_object, empty )
{
	EXPECT_TRUE( json_object().empty() );
	EXPECT_TRUE( json_object( "{}" ).empty() );
	EXPECT_TRUE( json_object( "{ }" ).empty() );
	EXPECT_FALSE( json_object( example1 ).empty() );
	EXPECT_FALSE( json_object( example2 ).empty() );
	EXPECT_FALSE( json_object( example3 ).empty() );
	EXPECT_FALSE( json_object( example4 ).empty() );
	EXPECT_FALSE( json_object( example5 ).empty() );
}

TEST( json_object, size )
{
	EXPECT_EQ( json_object().size(), 0 );
	EXPECT_EQ( json_object( "{}" ).size(), 0 );
	EXPECT_EQ( json_object( "{ }" ).size(), 0 );
	EXPECT_EQ( json_object( strings ).size(), 1 );
	EXPECT_EQ( json_object( numbers ).size(), 4 );
	EXPECT_EQ( json_object( example1 ).size(), 1 );
	EXPECT_EQ( json_object( example2 ).size(), 1 );
	EXPECT_EQ( json_object( example3 ).size(), 1 );
	EXPECT_EQ( json_object( example4 ).size(), 1 );
	EXPECT_EQ( json_object( example5 ).size(), 1 );
}

TEST( json_object, clear )
{
	json_object json( numbers );
	json.clear();

	EXPECT_TRUE( json.empty() );
	EXPECT_EQ( json.size(), 0 );
	EXPECT_TRUE( json.valid() );
}

TEST( json_object, valid )
{
	EXPECT_TRUE( json_object().valid() );
	EXPECT_TRUE( json_object( strings ).valid() );
	EXPECT_TRUE( json_object( numbers ).valid() );
	EXPECT_TRUE( json_object( example1 ).valid() );
	EXPECT_TRUE( json_object( example2 ).valid() );
	EXPECT_TRUE( json_object( example3 ).valid() );
	EXPECT_TRUE( json_object( example4 ).valid() );
	EXPECT_TRUE( json_object( example5 ).valid() );

	EXPECT_TRUE( json_object( "{}" ).valid() );
	EXPECT_FALSE( json_object( "{" ).valid() );
	EXPECT_FALSE( json_object( "[]" ).valid() );
	EXPECT_FALSE( json_object( "[" ).valid() );
	EXPECT_FALSE( json_object( "\"\"" ).valid() );
	EXPECT_FALSE( json_object( "\"" ).valid() );
	EXPECT_FALSE( json_object( "-1.23e+10" ).valid() );
	EXPECT_FALSE( json_object( "true" ).valid() );
	EXPECT_FALSE( json_object( "false" ).valid() );
	EXPECT_FALSE( json_object( "null" ).valid() );
}

