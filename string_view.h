/**
 * @file string-view.h
 * @brief A string view for C++11
 * @author Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @copyright 2023 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 * @version 2023-10-28
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#ifndef DULL_STRING_VIEW_H
#define DULL_STRING_VIEW_H

#if __cplusplus >= 201703L
#include <string_view>
#else
#include <string>
#include <cstring>
#endif

namespace dull
{

#if __cplusplus >= 201703L

using string_view = std::string_view;

#else

class string_view
{
	const char *str;
	size_t len;

public:
	using value_type             = char;
	using pointer                = value_type*;
	using const_pointer          = const value_type*;
	using reference              = value_type&;
	using const_reference        = const value_type&;
	using const_iterator         = const value_type*;
	using iterator               = const_iterator;
	using const_reverse_iterator = std::reverse_iterator<const_iterator>;
	using reverse_iterator       = const_reverse_iterator;
	using size_type              = std::size_t;
	using difference_type        = std::ptrdiff_t;

	// Constructors

	string_view() : str { nullptr }, len { 0 } {}
	string_view( const char *data, size_type length ) : str { data }, len { length } {}

	// cppcheck-suppress noExplicitConstructor
	string_view( const char *data ) : str { data }, len { std::strlen( data ) } {}

	// cppcheck-suppress noExplicitConstructor
	string_view( const std::string &str ) : str { str.c_str() }, len { str.length() } {}

	// Iterators

	const_iterator begin() const noexcept { return str; }
	const_iterator cbegin() const noexcept { return str; }

	const_iterator end() const noexcept { return str + len; }
	const_iterator cend() const noexcept { return str + len; }

	const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator( end() ); }
	const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator( end() ); }

	const_reverse_iterator rend() const noexcept { return const_reverse_iterator( begin() ); }
	const_reverse_iterator crend() const noexcept { return const_reverse_iterator( begin() ); }

	// Element access

	const_reference operator[]( size_type pos ) const noexcept
	{
		return str[pos];
	}

	const_reference front() const noexcept { return str[0]; }
	const_reference back() const noexcept { return str[len - 1]; }
	const_pointer data() const noexcept { return str; }

	// Capacity

	size_type size() const noexcept { return len; }
	size_type length() const noexcept { return len; }
	bool empty() const noexcept { return len == 0; }

	// Modifiers

	void remove_prefix( size_type n ) noexcept { str += n; len -= n; }
	void remove_suffix( size_type n ) noexcept { len -= n; }

	// Operations

	size_t find( char ch, size_type pos = 0 ) const noexcept
	{
		for ( ; pos < length(); ++pos ) if ( str[pos] == ch ) return pos;
		return npos;
	}

	// Constants

	static constexpr size_type npos = static_cast<size_type>( -1 );

	// Comparison operators

	bool operator==( const string_view &other ) const noexcept
	{
		if ( length() != other.length() ) return false;
		return std::strncmp( data(), other.data(), length() ) == 0;
	}

	bool operator!=( const string_view &other ) const noexcept
	{
		return !operator==( other );
	}
};

#endif

}

#endif

